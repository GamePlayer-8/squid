#!/bin/sh

SCRIPT_PATH="$(dirname "$(realpath "$0")")"

cd "$SCRIPT_PATH"/source

export MAKEFLAGS='-j'"$(nproc --all)"

make clean

./bootstrap.sh
./configure --with-openssl \
	--prefix=/app \
	--enable-async-io \
	--disable-icmp \
	--enable-useragent-log \
	--enable-delay-pools \
	--enable-kill-parent-hack \
	--enable-forw-via-db \
	--enable-htpc \
	--enable-carp \
	--enable-snmp \
	--enable-cache-digests \
	--without-systemd \
	--without-gnugss \
	--enable-ssl-crtd

make
make install

make clean
